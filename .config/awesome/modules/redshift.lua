--- Redshift for Awesome WM
-- Copyright (c) 2013  Ryan Young <ryan .ry. young@gmail. com> (omit spaces)
--
-- https://github.com/troglobit/awesome-redshift
--

-- standard libraries
local awful = require("awful")

-- variables
local redshift = {}
redshift.redshift = "/usr/bin/redshift"    -- binary path
redshift.method   = "randr"                -- randr or vidmode
redshift.options  = ""                     -- additional redshift command options
redshift.state    = 1                      -- 1 for screen dimming, 0 for none
-- redshift.timer    = timer({ timeout = 60 })
redshift.temperature = 3200
redshift.max = 6500
redshift.min = 1000

-- Uncomment all the lines with the timer and automatically enable redshift if you'd like redshift to handle dimming automatically

-- functions
function redshift.dim(temperature)
  if redshift.method == "randr" then
    awful.util.spawn(redshift.redshift .. " -m randr -o " .. redshift.options .. "-O" .. tostring(temperature))
  elseif redshift.method == "vidmode" then
    local screens = screen.count()
    for i = 0, screens - 1 do
      awful.util.spawn(redshift.redshift .. " -m vidmode:screen=" .. i ..
                       " -o " .. redshift.options)
    end
  end
  redshift.state = 1
  -- redshift.timer:start()
end

-- redshift.timer:connect_signal("timeout", function() redshift.dim(redshift.temperature) end)
function redshift.undim()
  if redshift.method == "randr" then
    awful.util.spawn(redshift.redshift .. " -m randr -x " .. redshift.options)
  elseif redshift.method == "vidmode" then
    local screens = screen.count()
    for i = 0, screens - 1 do
      awful.util.spawn(redshift.redshift .. " -m vidmode:screen=" .. i ..
                       " -x " .. redshift.options)
    end
  end
  redshift.state = 0
  -- redshift.timer:stop()
end

function redshift.tempUP(interval)

  -- Make sure the redshift temperature does not go past the maximum value
  if redshift.temperature >= (redshift.max - interval) then
    redshift.temperature = redshift.max
  else
    redshift.temperature = redshift.temperature + interval
  end


  -- If redshift is on, restart the process to update the temperature
  if redshift.state == 1 then
    redshift.undim()
    redshift.dim(redshift.temperature)
  end

end

function redshift.tempDOWN(interval)

  -- Make sure the redshift temperature does not go under the minimum value
  if redshift.temperature <= (redshift.min + interval) then
    redshift.temperature = redshift.min
  else
    redshift.temperature = redshift.temperature - interval
  end

  -- If redshift is on, restart the process to update the temperature
  if redshift.state == 1 then
    redshift.undim()
    redshift.dim(redshift.temperature)
  end

end

function redshift.toggle()
  if redshift.state == 1 then
    redshift.undim()
  else
    redshift.dim(redshift.temperature)
  end
end

function redshift.init(state)
  if state == 1 then
    redshift.undim()
    redshift.dim(redshift.temperature)
  else
    redshift.undim()
  end
end

redshift.init(redshift.state)

return redshift
