local wibox = require("wibox")

-- Create a textclock widget
mytextclock = wibox.widget.textclock("%Y-%m-%d %H:%M:%S ", 1)
