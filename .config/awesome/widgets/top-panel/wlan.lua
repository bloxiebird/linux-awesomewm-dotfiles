local wibox = require('wibox')
local awful = require('awful')
local gears = require('gears')

local wlan_buttons = gears.table.join(
  awful.button({ }, 1, function(t)
      awful.spawn.with_shell(". ~/.config/awesome/configuration/util/rofiwifi/rofi-network-manager.sh")
  end)
)

wlan = wibox.widget {
    text   = 'Not Connected!',
    align = 'center',
    valign = 'center',
    buttons = wlan_buttons,
    widget = wibox.widget.textbox,
}

-- For the SSID Checker
awful.widget.watch(
  -- For without iwgetid, found in the arch wireless_tools package.
	-- [[bash -c "nmcli -t -f active,ssid dev wifi | grep '^yes' | cut -d: -f2"]],
  'bash -c "iwgetid -r"',
	10,
	function(_, stdout)
    if string.len(stdout) > 1  then
      wlan.text = (stdout)
    else
      wlan.text = ("Not Connected!")
    end

		collectgarbage('collect')
	end
)
