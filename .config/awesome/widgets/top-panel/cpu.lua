local wibox = require('wibox')
local awful = require('awful')
local gears = require('gears')

local cpu_buttons = gears.table.join(
  awful.button({ }, 1, function(t)
      awful.spawn.with_shell("termite -e htop")
  end)
)

cpu = wibox.widget {
  text   = 'Cant detect CPU!',
  align = 'center',
  valign = 'center',
  buttons = cpu_buttons,
  widget = wibox.widget.textbox,
}

-- Not particularly sure why the previous total/idle is needed.
idle_prev  = 0
total_prev = 0
-- For the CPU Bar -- Credits to the Material Awesome/Floppier Git Projects for this. --
awful.widget.watch(
	[[bash -c "
    cat /proc/stat | grep '^cpu '
    "]],
    5,
    function(_, stdout)
      local user, nice, system, idle, iowait, irq, softirq, steal, guest, guest_nice =
        stdout:match('(%d+)%s(%d+)%s(%d+)%s(%d+)%s(%d+)%s(%d+)%s(%d+)%s(%d+)%s(%d+)%s(%d+)%s')

      local total = user + nice + system + idle + iowait + irq + softirq + steal

      local diff_idle = idle - idle_prev
      local diff_total = total - total_prev
      local diff_usage = (1000 * (diff_total - diff_idle) / diff_total + 5) / 10

  		cpu.text = ("CPU Usage : " .. (math.floor(diff_usage * 10) / 10) .. "%")

      total_prev = total
      idle_prev = idle
      collectgarbage('collect')
    end
)
