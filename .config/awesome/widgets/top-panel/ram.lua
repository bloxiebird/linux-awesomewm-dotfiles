local wibox = require('wibox')
local awful = require('awful')
local gears = require('gears')

local ram_buttons = gears.table.join(
  awful.button({ }, 1, function(t)
      awful.spawn.with_shell("termite -e htop")
  end)
)

ram = wibox.widget {
  text   = 'Cant detect RAM!',
  align = 'center',
  valign = 'center',
  buttons = ram_buttons,
  widget = wibox.widget.textbox,
}

-- For the Memory Bar -- Credits to the Material Awesome/Floppier Git Projects for this.
awful.widget.watch(
	'bash -c "free -m | grep -z Mem.*Swap.*"',
	5,
	function(_, stdout)
		local total, used, free, shared, buff_cache, available, total_swap, used_swap, free_swap =
			stdout:match('(%d+)%s*(%d+)%s*(%d+)%s*(%d+)%s*(%d+)%s*(%d+)%s*Swap:%s*(%d+)%s*(%d+)%s*(%d+)')
		ram.text = ("Ram Usage : " .. used .. " MB")
		collectgarbage('collect')
	end
)
